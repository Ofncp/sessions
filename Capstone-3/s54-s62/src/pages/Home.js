import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
  const jumbotronData = {
    title: "Icebound Gears Hub",
    content: "Gear Up, Skate Hard, Score Big!",
    titleClassName: 'jumbotron-title',
    contentClassName: 'jumbotron-content'
  };

  const bannerData = {
    destination: "/products",
    label: "Enter Shop",
    className: 'banner'
  };

  return (
    <div className="d-flex flex-column justify-content-between" style={{ minHeight: '100vh' }}>
      <div className="jumbotron text-center">
        <div>
          <h1 className={jumbotronData.titleClassName}>{jumbotronData.title}</h1>
          <p className={jumbotronData.contentClassName}>{jumbotronData.content}</p>
        </div>
      </div>
      <div>
        <Banner data={bannerData} />
      </div>
      <div className="mt-3 highlights-container">
        <Highlights className="highlights" />
      </div>
    </div>
  );
}
