import React, { useState } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import productsDataObject from '../data/productsData';
// import { productsData, productImageMapping } from '../data/productsData';

export default function ProductView() {
  const [products, setProducts] = useState(productsDataObject.productsData); 
  

  const colors = ["Select a Color", "Red", "Blue", "Green", "Black", "White"];

  const handleColorChange = (productIndex, e) => {
    const updatedProducts = [...products];
    updatedProducts[productIndex].selectedColor = e.target.value;
    setProducts(updatedProducts);
  };

  const buy = (productIndex) => {
    const selectedColor = products[productIndex].selectedColor;

    if (selectedColor === 'Select a Color') {
      alert('Please select a color before buying.');
      return;
    }

    // Simulate a successful purchase
    const updatedProducts = [...products];
    updatedProducts[productIndex].isPurchased = true;
    setProducts(updatedProducts);

    // You can also reset the selected color or perform other actions as needed.
  };

  return (
    <Container className="mt-5">
      <Row>
        {products.map((product, index) => (
          <Col key={index} lg={4} md={6} sm={12}>
            <Card style={{ height: '100%' }}>
              <Card.Img
                src={product.image} // Ensure that product.image is correctly defined in your data.
                alt={product.name}
                style={{ height: '50%' }}
              />

              <Card.Body className="text-center" style={{ height: '50%' }}>
                <Card.Title>{product.name}</Card.Title>
                <Card.Subtitle className="mb-2">Description:</Card.Subtitle>
                <Card.Text className="mb-2">{product.description}</Card.Text>
                <Card.Subtitle className="mb-2">Price:</Card.Subtitle>
                <Card.Text className="mb-2">{`PhP ${product.price}`}</Card.Text>
                <Card.Subtitle className="mb-2">Select Color:</Card.Subtitle>
                <select value={product.selectedColor} onChange={(e) => handleColorChange(index, e)} className="mb-2">
                  {colors.map((color, colorIndex) => (
                    <option key={colorIndex} value={color}>
                      {color}
                    </option>
                  ))}
                </select>
                <div className="mt-2">
                  <Button
                    variant="primary"
                    block
                    onClick={() => buy(index)}
                    disabled={product.isPurchased}
                  >
                    {product.isPurchased ? 'Purchased' : 'Buy'}
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
}
