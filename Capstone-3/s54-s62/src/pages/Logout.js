import { Navigate } from 'react-router-dom';
//destructures react to use useContext, useEffect, useStaate without dot notation
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';

export default function Logout() {


    const { unsetUser, setUser} = useContext(UserContext);
    //updates localStorage to empty / clear the storage
    unsetUser();


    useEffect(() => {
        // setUser({access: null});
       
       setUser({
        id: null,
        isAdmin: null

       })
    });


    localStorage.clear();

    // Redirect back to login
    return (
        <Navigate to='/login' />
    )

}













