import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom'; // Import Navigate
import EditProduct from '../components/EditProduct';

export default function AdminView() {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]); // Define products state variable

  useEffect(() => {
    // Fetch product data from your backend
    fetch('https://capstone2b-ocampo.onrender.com/products', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(data); // Store the fetched products in the state
      })
      .catch((error) => {
        console.error('Error fetching product data: ', error);
      });
  }, []);

  if (!user.isAdmin) {
    // Redirect non-admin users
    return <Navigate to="/products" />;
  }

  return (
    <Container className="mt-5">
      <h1 className="text-center">Admin View</h1>
      <Row>
        {products.length > 0 ? (
          products.map((product) => (
            <Col key={product._id} lg={4} className="mb-4">
              <Card>
                <Card.Body>
                  <Card.Title>{product.name}</Card.Title>
                  <Card.Text>{product.description}</Card.Text>
                  <Card.Text>Price: {product.price}</Card.Text>
                  <EditProduct product={product} />
                </Card.Body>
              </Card>
            </Col>
          ))
        ) : (
          <p>Loading product data...</p>
        )}
      </Row>
    </Container>
  );
}
