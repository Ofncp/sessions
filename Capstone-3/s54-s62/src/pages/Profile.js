import React, { useState, useEffect, useContext } from 'react';
import { Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import UpdateProfileForm from '../components/UpdateProfileForm';
import ResetPassword from '../components/ResetPassword'; 

export default function Profile() {
  const { user } = useContext(UserContext);
  const [details, setDetails] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(`https://capstone2b-ocampo.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data._id !== 'undefined') {
          setDetails(data);
        }
      })
      .catch((error) => {
        console.error('Error fetching user details:', error);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const handleProfileUpdate = (updatedDetails) => {
    fetch('https://capstone2b-ocampo.onrender.com/users/details', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(updatedDetails),
    })
      .then((res) => res.json())
      .then((data) => {
        setDetails(data); // Update the state with the response from the server
      })
      .catch((error) => {
        console.error('Error updating profile:', error);
        // Handle error here
      });
  };

  return user.id === null ? (
    <Navigate to="/products" />
  ) : (
    <>
      {loading ? (
        <p>Loading...</p>
      ) : (
        <>
          <Row>
            <Col>
              <h1>{`${details.firstName} ${details.lastName}`}</h1>
              <h4>Email: {details.email}</h4>
              <h4>Mobile No: {details.mobileNo}</h4>
            </Col>
          </Row>
          <Row className="pt-4 mt-4">
            <Col>
              <UpdateProfileForm onUpdate={handleProfileUpdate} />
            </Col>
          </Row>
          {/* Add the ResetPassword component */}
          <Row className="pt-4 mt-4">
            <Col>
              <ResetPassword />
            </Col>
          </Row>
        </>
      )}
    </>
  );
}
