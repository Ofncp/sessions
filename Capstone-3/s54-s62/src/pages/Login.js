import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login() {
  const { user, setUser } = useContext(UserContext);

  // State hooks to store the values of the input fields
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(true);

  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function authenticate(e) {
    // Prevents page redirection via form submission
    e.preventDefault();
    fetch('https://capstone2b-ocampo.onrender.com/users/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);
          Swal.fire({
            title: 'Login Successful!',
            icon: 'success',
            text: 'Icebound Gears Hub!',
          });
          // Check if the user is an admin
          if (data.isAdmin) {
            // Redirect to AdminView for admin users
            return <Navigate to="/adminView" />;
          } else {
            // Redirect to Products for non-admin users
            return <Navigate to="/products" />;
          }
        } else {
          Swal.fire({
            title: 'Authentication Failed!',
            icon: 'error',
            text: 'Check your login details and try again!',
          });
        }
      });
  }

  const retrieveUserDetails = (token) => {
    fetch('https://capstone2b-ocampo.onrender.com/users/details', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  // If the user is already logged in, check if they are an admin and redirect accordingly
  if (user.id !== null) {
    if (user.isAdmin) {
      return <Navigate to="/adminView" />;
    } else {
      return <Navigate to="/products" />;
    }
  } else {
    // Display the login form for users who are not logged in
    return (
      <Form onSubmit={(e) => authenticate(e)}>
        <h1 className="form-title my-5 text-center">Login</h1>
        <Form.Group controlId="userEmail" className="mb-3">
         <Form.Label className="form-label"></Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className="form-input" 
          />
        </Form.Group>
        <Form.Group controlId="password" className="mb-3">
          <Form.Label className="form-label"></Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            className="form-input" 
          />
        </Form.Group>
        <Button variant="primary" type="submit" id="submitBtn" className="form-submit-btn">Submit</Button>
      </Form>
    );
  }
}
