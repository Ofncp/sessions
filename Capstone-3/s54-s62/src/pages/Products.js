import React, { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import productsData from '../data/productsData';

const ProductView = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const fetchProductsData = async () => {
      try {
        // Example API call
        const response = await fetch(`${process.env.REACT_APP_API_URL}/products/`);
        const data = await response.json();

        if (Array.isArray(data)) {
          setProducts(data);
        } else {
          console.error("API response is not an array:", data);
          // Handle the non-array response, setProducts([]) or handle it accordingly
        }
      } catch (error) {
        console.error("Error fetching products:", error);
        // Handle the error, setProducts([]) or display an error message
      }
    };

    // Call the fetch function
    fetchProductsData();
  }, []);

  useEffect(() => {
    console.log("Products:", products);
  }, [products]); // Log products whenever it changes

  const colors = ["Select a Color", "Red", "Blue", "Green", "Black", "White"];

  const handleColorChange = (productIndex, e) => {
    const updatedProducts = [...products];
    updatedProducts[productIndex].selectedColor = e.target.value;
    setProducts(updatedProducts);
  };

  const buy = (productIndex) => {
    const selectedColor = products[productIndex].selectedColor;

    if (selectedColor === 'Select a Color') {
      alert('Please select a color before buying.');
      return;
    }

    // Simulate a successful purchase
    const updatedProducts = [...products];
    updatedProducts[productIndex].isPurchased = true;
    setProducts(updatedProducts);
  };

  return (
    <Container className="mt-5">
      <Row>
        {products.map((product, index) => (
          <Col key={index} lg={4} md={6} sm={12}>
            <Card style={{ height: '100%' }}>
              <Card.Img
                src={product.image}
                alt={product.name}
                style={{ height: '50%' }}
              />

              <Card.Body className="text-center" style={{ height: '50%' }}>
                <Card.Title>{product.name}</Card.Title>
                <Card.Subtitle className="mb-2">Description:</Card.Subtitle>
                <Card.Text className="mb-2">{product.description}</Card.Text>
                <Card.Subtitle className="mb-2">Price:</Card.Subtitle>
                <Card.Text className="mb-2">{`PhP ${product.price}`}</Card.Text>
                <Card.Subtitle className="mb-2">Select Color:</Card.Subtitle>
                <select
                  value={product.selectedColor}
                  onChange={(e) => handleColorChange(index, e)}
                  className="mb-2"
                >
                  {colors.map((color, colorIndex) => (
                    <option key={colorIndex} value={color}>
                      {color}
                    </option>
                  ))}
                </select>
                <div className="mt-2">
                  <Button
                    variant="primary"
                    block
                    onClick={() => buy(index)}
                    disabled={product.isPurchased}
                  >
                    {product.isPurchased ? 'Purchased' : 'Buy'}
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default ProductView;
