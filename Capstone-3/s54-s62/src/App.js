// import React, { Component } from 'react';
import AppNavbar from './components/AppNavbar';
import Products from './pages/Products';
import Home from './pages/Home';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';

// BrowserRouter, Routes, Route works together to setup components endpoints
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import {useState, useEffect} from 'react';
import { Container } from 'react-bootstrap';
import './App.css';

import Register from './pages/Register'
import ProductView from './pages/ProductView'
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Profile from './pages/Profile';
import AdminView from './pages/AdminView';
import AddProduct from './pages/AddProduct';


import {UserProvider} from './UserContext';

function App() {


  // Gets the token from local storage and assigns the token to user state
  const[user, setUser] = useState({
  //token: localStorage.getItem('token')
    id: null,
    isAdmin: null
  });


  // unsetUser is a function for clearing local storage
  const unsetUser = () => {
    localStorage.clear();
  }

  // Check if user info is properly stored upon login and the localStorage if cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    // The user state, setUser (setter function) and unsetUser function is provided in the UserContext to be shared to other pages/components.
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <Container>
          <AppNavbar />
          <Routes>
              <Route path="/" element = {<Home />} />

              <Route path="/products" element = {<Products />} />

              <Route path="/login" element = {<Login />} />

              <Route path="/register" element = {<Register />} />

              <Route path="/products/" element = {<ProductView />} />

              <Route path="/adminView" element = {<AdminView />} />

              <Route path="/addProduct" element = {<AddProduct />} />

              <Route path="/profile" element = {<Profile />} /> 

              <Route path="/logout" element = {<Logout />} /> 

              <Route path="/*" element = {<Error />} />   
          </Routes>
        </Container>
      </Router>
    </UserProvider>
   
  );

    
}

export default App;
