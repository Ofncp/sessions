
const productImageMapping = {
  "Hockey Jersey": "/images/Hockey_Jersey.png",
  "Hockey Pro V8 Stick": "/images/Hockey_Pro_V8_Stick.png",
  "Hockey Elbow Pad Pro 8": "/images/Hockey_Elbow_Pad_Pro_8.png",
  "Hockey Pro V8 Gloves": "/images/Hockey_Pro_V8_Gloves.png",
  "Hockey Pro V8 Helmet": "/images/Hockey_Pro_V8_Helmet.png",
  "Hockey Pro V8 Pants": "/images/Hockey_Pro_V8_Pants.png",
  "Hockey Pro V8 Shin Guards": "/images/Hockey_Pro_V8_Shin_Guards.png",
  "Hockey Pro V8 Shoulder Pad": "/images/Hockey_Pro_V8_Shoulder_Pad.png",
  "Hockey Pro V8 Skates": "/images/Hockey_Pro_V8_Skates.png",
  "Hockey Pro V8 Socks": "/images/Hockey_Pro_V8_Socks.png",
  "Goalie Pad": "/images/goalie_pad.PNG",
  "Hockey Comfy Jacket": "/images/Hockey_Comfy_Jacket.PNG",
};

const productsData = [
  {
    id: "652521ef8e09243c18754cf7",
    name: "Hockey Jersey",
    description: "Its woven neck tape prevents irritation while its breathable fabric keeps you cool while you work on your skill.",
    price: 1000,
    selectedColor: 'Select a Color',
    isPurchased: false,
    onOffer: true,
    image : "/images/Hockey_Jersey.png"
  },
  {
    id: "6525223d8e09243c18754cfa",
    name: "Hockey Pro V8 Stick",
    description: "It's designed with an optimized low kickpoint that enables you to unleash the ideal shot, and the Dual Feel blade technology, which consists of an avant-garde foam insert in the heel of the blade, greatly enhances your feel. Advanced carbon stacking processes are used in the Nanolite Shield technology, which results in a significant weight reduction without compromising durability.",
    price: 17500,
    selectedColor: 'Select a Color',
    isPurchased: false,
    onOffer: true,
    image : "/images/Hockey _Pro _V8_ Stick.png"
  },
   {
    id: "652522e48e09243c18754cfd",
    name: "Hockey Elbow Pad Pro 8",
    description: "Even if you only use the ice occasionally, comfort should be personalized. These comfortable elbow guards from the NEXT line are easily adjustable. Furthermore, they are made to be used as frequently (or as infrequently) as desired by casual players of any age!",
    price: 2250,
    selectedColor: 'Select a Color',
    isPurchased: false,
    onOffer: true,
    image : "/images/elbowpads.png",
  },
  {
    id: "6525238d8e09243c18754d00",
    name: "Hockey Pro V8 Gloves",
    description: "They provide elite-level protection with unparalleled mobility thanks to its Anatomical Shield Design construction and three-piece index with high-density EVA foam. This also allows for improved airflow, which keeps your hands cool and nimble as you dangle on the ice.",
    price: 2250,
    selectedColor: 'Select a Color',
    isPurchased: false,
    onOffer: true,
    image : "/images/Hockey_Gloves.png",
  },
  {
    id: "6525241a8e09243c18754d03",
    name: "Hockey Pro V8 Helmet",
    description: "This helmet has excellent ventilation channels that ensure a smooth flow of air. Maximum impact energy dispersal is also ensured. Whether you're carving a path through the middle of the ice or squaring up on the corners, you can rely on the TACKS 720 to keep you calm and focused while safeguarding your most precious possession.",
    price: 12000,
    selectedColor: 'Select a Color',
    isPurchased: false,
    onOffer: true,
    image : "/images/Hockey_Helmet.png",
  },
  {
    id: "652524a28e09243c18754d06",
    name: "Hockey Pro V8 Pants",
    description: "Move. Faster. Instinctively. Thanks to the incorporation of AER-TEC technology into the existing timeless design, hockey pants are the pinnacle of lightweight and flexible protective apparel. While the anti-twist belt keeps these breezers in place as you blow past your opponents, strategic ventilation improves moisture evaporation.",
    price: 11500,
    selectedColor: 'Select a Color',
    isPurchased: false,
    onOffer: true,
    image : "/images/Hockey_Pants.png",
  },
  {
    id: "652525398e09243c18754d09",
    name: "Hockey Pro V8 Shin Guards",
    description: "Optimal comfort is guaranteed by a light fit that doesn't sacrifice protection. They are made for casual players who want freedom of movement while protecting themselves on the ice thanks to their design and construction.",
    price: 2750,
    selectedColor: 'Select a Color',
    isPurchased: false,
    onOffer: true,
    image : "/images/Hockey_ShinGuards.png",
  },
  {
    id: "652526198e09243c18754d0c",
    name: "Hockey Pro V8 Shoulder Pad",
    description: "With shoulder pads, you can keep your torso bone dry and your head firmly in the game. Their ground-breaking AER-TEC design effectively removes humidity, and their incredibly light EPP foam core completely protects against impact, so there won't be any technical difficulties getting in the way of your success.",
    price: 2750,
    selectedColor: 'Select a Color',
    isPurchased: false,
    onOffer: true,
    image : "/images/Hockey_Shoulderpad.png",
  },
  {
    id: "652526938e09243c18754d0f",
    name: "Hockey Pro V8 Skates",
    description: "Their feet fit it perfectly thanks to its anatomical design, and the reinforced boot gives them the support they need to perform at their absolute best.",
    price: 11500,
    selectedColor: 'Select a Color',
    isPurchased: false,
    onOffer: true,
    image : "/images/Hockey_Skates.png",
  },
  {
    id: "652527238e09243c18754d12",
    name: "Hockey Pro V8 Socks",
    description: "The 100% breathable polyester fabric of the high-performance practice sock is equipped with Velcro fasteners to ensure that it stays in place while also wicking moisture away from your feet. These hockey socks are made to keep you feeling light and comfortable, and they have inserts that improve stretch and mobility.",
    price: 1650,
    selectedColor: 'Select a Color',
    isPurchased: false,
    onOffer: true,
    image : "/images/Hockey_Socks.png",
  },
  {
    id: "653a08713c946154d43562d6",
    name: "Goalie Pad",
    description: "Their reactivity and solid seal to the ice means you can stack the pads without sacrificing your range of motion.",
    price: 13000,
    selectedColor: 'Select a Color',
    isPurchased: false,
    onOffer: true,
    image : "/images/goalie_pad.PNG",
  },
  {
    id: "653768b61135b05f911e3e73",
    name: "Hockey Comfy Jacket",
    description: "The 100% breathable polyester fabric of the high-performance practice sock is equipped with Velcro fasteners to ensure that it stays in place while also wicking moisture away from your feet. These hockey socks are made to keep you feeling light and comfortable, and they have inserts that improve stretch and mobility.",
    price: 1650,
    selectedColor: 'Select a Color',
    isPurchased: false,
    onOffer: true,
    image : "/images/Hockey_Comfy_Jacket.PNG",
  },

  // Add other product data...
];

// Assign image source to each product
productsData.forEach((product, count) => {
  // product.image = productImageMapping[product.name][count++];
  console.log(product);
  console.log(count);
});

export default { productsData, productImageMapping };