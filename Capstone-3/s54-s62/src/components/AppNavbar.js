import React, { useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import '../App.css';

export default function AppNavbar() {
    const { user } = useContext(UserContext);

    return (
        <Navbar className="transparent-navbar" expand="lg">
            <Container fluid>
                <Navbar.Brand as={Link} to="/">
                    <img
                        src="/images/logo.png"
                        alt="Icebound Logo"
                        width="80"
                        height="80"
                        className="d-inline-block align-top"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link as={NavLink} to="/" exact activeClassName="active">Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/products" exact activeClassName="active">Products</Nav.Link>
                        {(user.id !== null) ? 
                            user.isAdmin ? (
                                <>
                                    <Nav.Link as={NavLink} to="/addProduct" activeClassName="active">Add Product</Nav.Link>
                                    <Nav.Link as={NavLink} to="/profile" activeClassName="active">Profile</Nav.Link>
                                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                                </>
                            ) : (
                                <>
                                    <Nav.Link as={NavLink} to="/profile" activeClassName="active">Profile</Nav.Link>
                                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                                </>
                            ) : (
                                <>
                                    <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                                    <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                                </>
                            )
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}
