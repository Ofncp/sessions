// [ SECTION ] Dependecies and Modules
const mongoose = require('mongoose');

// [ SECTION ] Schema/Blueprint
const productSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [ true, 'Product is required!' ]
	},
	description : {
		type: String,
		required: [ true, 'Description is required!' ]
	},
	price : {
		type: Number,
		required: [ true, 'Price is required!' ]
	},
	isActive : {
		type: Boolean,
		default: true
	},
	createdOn : {
		type: Date,
		// The "new Date()" expression that instantiates a new "date" that store the current time and date whenever a product is created in our database
		default: new Date()
	},
	customers : [
		{
			userId : {
				type: String,
				required: [ true, 'UserId is required!' ]
			},
			memberOn : {
				type: Date,
				default: new Date()
			}
		}
	]
});

// [ SECTION ] Model
module.exports = mongoose.model('Product', productSchema);