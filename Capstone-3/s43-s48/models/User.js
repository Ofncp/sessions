//[SECTION] Modules and Dependencies
const mongoose = require("mongoose");

//[SECTION] Schema/Blueprint
const userSchema = new mongoose.Schema({

    firstName : {
        type : String,
        required : [true, "First name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNo : {
        type : String, 
        required : [true, "Mobile No is required"]
    },
    // The "membership" property/field will be an array of objects containing the product IDs, the date and time that the user purchased a product and the status that indicates if the user is currently purchased a product
    membership : [
        {
            productId : {
                type : String,
                required : [true, "Product ID is required"]
            },
            purchaseOn : {
                type : Date,
                default : new Date()
            },
            status : {
                type : String,
                default : "Purchased"
            }
        }
    ]
})

//[SECTION] Model
module.exports = mongoose.model("User", userSchema);