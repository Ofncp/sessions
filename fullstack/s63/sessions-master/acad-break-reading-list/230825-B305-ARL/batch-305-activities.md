# 230825 - Batch 305 (Sir Rome):  Academic Reading List 


### **Topics**

- MongoDB
  - [MongoDB](https://www.mongodb.com/)
- MongoDB Queries
  - [tutorialspoint](https://www.tutorialspoint.com/mongodb/mongodb_query_document.htm)
  - [mongodb](https://www.mongodb.com/docs/manual/reference/method/db.collection.aggregate/)
- Node JS
  - [nodejs](https://nodejs.org/en/docs)
- Express JS
- [expressjs](https://expressjs.com/en/5x/api.html)




### **Purpose**
- Review and practive how mongoDB works
- Master CRUD Operations using mongoDB Queries.
- Advance review for Node.js and Express.js


### **Goal to Checking**

#### In your Academic Break Reading List Folder, create a folder named 230825:

1. Try these activities to practice your data management skills<br>

    -Perfom CRUD Operations<br>

### **Create (Insert):**<br>

Use use mydb to select a database.
Insert a document using db.myCollection.insertOne({ key: value }). <br>

### **Read (Find):**<br>

Use db.myCollection.find() to retrieve all documents.
Use db.myCollection.find({ key: value }) to find specific documents.
Use db.myCollection.findOne({ key: value }) to find a single document.<br>

### **Update (Update):**<br>


Use db.myCollection.updateOne({ key: value }, { $set: { newKey: newValue } }) to update a document.
Use db.myCollection.updateMany({ key: value }, { $set: { newKey: newValue } }) to update multiple documents.<br>

### **Delete (Delete):**<br>

Use db.myCollection.deleteOne({ key: value }) to delete a document.
Use db.myCollection.deleteMany({ key: value }) to delete multiple documents.