let collection = [];
// Write the queue functions below.

function print(){

    // console.log(collection);
    return collection;
}

function enqueue(item){
    // add an item
    collection.push(item);
    return collection;
}

function dequeue(){
    // remove an item
    if (!isEmpty()) {
        collection.shift();
        return collection;
    }

}

function front(){
    // get the front item
    if (!isEmpty()) {
        return collection [0];
    }
}

function size(){
    // get the size of the array
    return collection.length;

   
}

function isEmpty(){
    // check array if empty
    return collection.length === 0;

}




// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};