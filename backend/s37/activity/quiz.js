// 1. What directive is used by Node.js in loading the modules it needs?
		Node uses "require" allows to import external files into node.js application


// 2. What Node.js module contains a method for server creation?
			http provides the important functions  to create and interact with servers and clients
			where it has a method for server creation which is createServer()


// 3. What is the method of the http object responsible for creating a server using Node.js?

				createServer()

// 4. What method of the response object allows us to set status codes and content types?
    
					res.writeHead()

// 5. Where will console.log() output its contents when run in Node.js?
						terminal or command prompt


// 6. What property of the request object contains the address's endpoint?
				req.url