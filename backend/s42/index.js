const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute.js");

// server setup +middleware

const app = express();
const port = 4000;
app.use(express.json());

app.use(express.urlencoded({extended:true}));
// default endpoint
// localhost:4000/tasks
app.use("/tasks", taskRoute);

// DB Connection
mongoose.connect("mongodb+srv://admin:admin123@cluster0.3rtvu1l.mongodb.net/B305-to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
	//allows us to avoid any current or future errors while connecting to MongoDB

});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error!"));


db.once("open",() => console.log("We're Connected to the cloud database."));








// Server listening
if(require.main === module){
	app.listen(port, () =>console.log(`Server running at port ${port}`));
}