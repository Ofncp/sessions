// [ SECTION ] Dependencies and Modules
const express = require('express');
const auth = require("../auth.js");

const router = express.Router();
const courseController = require('../controllers/course.js');
// const auth =  require("../auth.js");



// Destructuring of verify and verifyAdmin

const { verify, verifyAdmin} = auth;


// create a course POST
// course registration routes
router.post("/", verify, verifyAdmin, courseController.addCourse);

// get all courses
router.get("/all",courseController.getAllCourses);

// Get all Active Course
router.get("/", courseController.getAllActive);

// get specific course by using ID
router.get("/:courseId", courseController.getCourse);

// updating a course(Admin only)
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);


// archiving a course(Admin only)
router.post("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

// activating a course
router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);
// router.post("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);




// [ SECTION ] Export Route System
module.exports = router;

