const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port =3001;


// MongoDB Connection using Mongoose

mongoose.connect("mongodb+srv://admin:admin123@cluster0.3rtvu1l.mongodb.net/B305-to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
	//allows us to avoid any current or future errors while connecting to MongoDB

});


// set notification for connection or failure
let db = mongoose.connection;
// if error occured, ouput in console.
db.on("error", console.error.bind(console, "Connection Error!"));

// If connection is successful, output in console
db.once("open",() => console.log("We're Connected to the cloud database."));

// [Section] Mongoose Schema
// Schemas determine the structure of the documents tobe written in the database

const taskSchema = new mongoose.Schema({
	// define fields with corresponsinf data type
	name: String,
	status: {
		type: String,
		default: "pending"
			// default -> equips predefined values
	}
})

// [Section] Models
const Task = mongoose.model("Task", taskSchema);


// Middlewares
// Allows your app to read json data type
//  Allows to read and accept data in form
app.use(express.json());
app.use(express.urlencoded({extended:true}))


// Task Routes
// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
    - If the task already exists in the database, we return an error
    - If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

// create task route
app.post("/tasks", (req, res) => {

	Task.find({name: req.body.name}).then((result, error)=>{
		// if document was found or already existing
		if(result != null && result.name === req.body.name){
			return res.send("Duplicate task found!");
		}else{
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save().then((savedTask, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					// if there is no error
					return res.status(201).send("New Task Created!");
				}
			})
		}
	})
})



//  get all task
app.get("/tasks", (req, res)=> {
	Task.find({}).then((result, error) =>{
		if(error){
			return console.error(error);
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})

})













// port listening
if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`))
}


module.exports = app;