const Order = require('../models/Order');
const User = require("../models/User");
const Product = require("../models/Product");
const auth = require('../auth');
const crypto = require("crypto");


//checkOutOrder

module.exports.checkOutOrder = async (req, res) => {

	if (req.user.isAdmin){
		return res.status(401).send("Action Forbidden")
	};

	const invoiceNumber = crypto.randomBytes(16).toString("hex");


	let productData = await Product.findById(req.body.productID).populate('name');
	

	let newOrder = new Order({
		invoiceNo: invoiceNumber,
		orderedBy: await User.findById(req.user.id).populate('userName'),
		product: productData,
		quantity: req.body.quantity,
		shippingAddress: req.body.shippingAddress,
		total: req.body.quantity * parseFloat(productData.price),
		// orderDetails: orderDetailsRaw,
	});

	console.log(newOrder)

	newOrder.save()
	.then(newOrder => res.send(newOrder))
	.catch(err => res.send(err));
	
};

// MY STRETCH GOALS-- START  not yet finish testing////

//View orders by Admin

module.exports.viewAllOrders = (req, res) => {

	Order.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

//View orders by User


module.exports.viewMyOrdersByInvoiceNo = (req, res) => {
  const { invoiceNo } = req.query;

  if (!invoiceNo) {
    return res.status(400).json({ error: "InvoiceNo is required." });
  }

  Order.findOne({ invoiceNo })
    .then((result) => {
      if (!result) {
        return res.status(404).json({ error: "Order not found." });
      }
      res.json(result);
    })
    .catch((err) => res.status(500).json({ error: "Internal server error." }));
};



/* Previous version
module.exports.viewMyOrders = (req, res) => {

	Order.findById(req.params.invoiceNo)
	.then(result => res.send(result))
	.catch(err => res.send(err));
}; */

// MY STRETCH GOALS-- END////