const express = require('express');

const auth = require("../auth.js");

const {verify, verifyAdmin} = auth;

const router = express.Router();

const userControllers = require('../controllers/userControllers')







//User Registration
router.post("/", userControllers.registerUser);

//Retrieve all users
router.get("/", verify, verifyAdmin, userControllers.getAllUsers);


//Log In route
router.post("/login", verify, verifyAdmin, userControllers.loginUser);

//Update to Admin
router.put("/updateAdmin/: id", verify, verifyAdmin, userControllers.updateAdmin);
	


//Get Single User
router.get("/:id", verify, userControllers.getSingleUser);




module.exports = router;