// Dependencies & Modules
const express = require('express');
const mongoose = require('mongoose');

const cors = require('cors');

require('dotenv').config();

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');



//Server Setup
const app = express();


//Environment Setup
const port = 4000;



//Database Connection
// mongoose.connect("mongodb+srv://admin:admin123@cluster0.3rtvu1l.mongodb.net/Capstone2-A",{
// 		useNewUrlParser: true,
// 		useUnifiedTopology: true
// });

mongoose.connect('mongodb+srv://admin:admin123@cluster0.3rtvu1l.mongodb.net/Session43-S48?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});  



let db = mongoose.connection;

db.on('error', console.error.bind(console, "Connection Error"));

db.once('open', () => console.log("Connected to MongoDB"));



//Backend Routes
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);


// // Error Handling Middleware
// app.use((err, req, res, next) => {
//   console.error(err);
//   res.status(500).json({ error: 'Internal server error' });
// });



// [ SECTION ] Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, () => {
		console.log(`API is now online on port ${ process.env.PORT || port }`);
	});
};




// app.listen(port, () => console.log(`Server is running on localhost: ${port}`))

module.exports = app;




