import { Row, Col, Card } from 'react-bootstrap';

export default function Hightlights(){
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Learn to Play</h2>
			        </Card.Title>
			        <Card.Text>
			          Get your flippers off and let's go. Take it one game at a time. Money on the board..
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Play Now, Pay Later</h2>
			        </Card.Title>
			        <Card.Text>
			          We wanna move the puck north south not east west. That was a goal scorer's goal! Just get the point.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Ice Warriors Community</h2>
			        </Card.Title>
			        <Card.Text>
			          Winning puck battles. It was a team effort. High forward.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
	)
}