import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
// import productsData from '../data/prodcutsData';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../pages/AdminView';

export default function Products() {

    const { user } = useContext(UserContext);

    // Checks to see if the mock data was captured
    // console.log(coursesData);
    // console.log(coursesData[0]);

    // State that will be used to store the products retrieved from the database
    const [products, setProducts] = useState([]);


    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/all`)
        .then(res => res.json())
        .then(data => {
            
            console.log(data);

            // Sets the "products" state to map the data retrieved from the fetch request into several "ProductCard" components
            setProducts(data);

        });
    }


    // Retrieves the products from the database upon initial render of the "Products" component
    useEffect(() => {

        fetchData()

    }, []);

    // The "map" method loops through the individual product objects in our array and returns a component for each product
    // Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed, added or removed
    // Everytime the map method loops through the data, it creates a "ProductCard" component and then passes the current element in our productsData array using the productProp
    // const products = productsData.map(product => {
    //     return (
    //         <ProductCard key={product.id} productProp={product}/>
    //     );
    // })

    return(
        <>
            {
                (user.isAdmin === true) ?
                    <AdminView productsData={products} fetchData={fetchData} />

                    :

                    <UserView productsData={products} />

            }
        </>
    )
}