import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
//import ProductCard from '../components/ProductCard';

export default function Home() {

    const data = {
        /* textual contents*/
        title: "Icebound Gears Hub",
        content: "Gear Up, Skate Hard, Score Big!",
        /* buttons */
        destination: "/products",
        label: "Enter Shop!"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>
    )
}