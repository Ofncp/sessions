const productsData = [
	{
		id: "652521ef8e09243c18754cf7",
		name: "Hockey Jersey",
		description: "Its woven neck tape prevents irritation while its breathable fabric keeps you cool while you work on your skill.",
		price: 1000,
		onOffer: true
	},
	{
		id: "6525223d8e09243c18754cfa",
		name: "Hockey Pro V8 Stick",
		description: "It's designed with an optimized low kickpoint that enables you to unleash the ideal shot, and the Dual Feel blade technology, which consists of an avant-garde foam insert in the heel of the blade, greatly enhances your feel. Advanced carbon stacking processes are used in the Nanolite Shield technology, which results in a significant weight reduction without compromising durability..",
		price: 17500,
		onOffer: true
	},
	{
		id: "652522e48e09243c18754cfd",
		name: "Hockey Elbow Pad Pro 8",
		description: "Even if you only use the ice occasionally, comfort should be personalized. These comfortable elbow guards from the NEXT line are easily adjustable. Furthermore, they are made to be used as frequently (or as infrequently) as desired by casual players of any age!.",
		price: 2250,
		onOffer: true
	},
	{
		id: "6525238d8e09243c18754d00",
		name: "Hockey Pro V8 Gloves",
		description: "They provide elite-level protection with unparalleled mobility thanks to its Anatomical Shield Design construction and three-piece index with high-density EVA foam. This also allows for improved airflow, which keeps your hands cool and nimble as you dangle on the ice.",
		price: 2250,
		onOffer: true
	},
	{

		id: "6525241a8e09243c18754d03",
		name: "Hockey Pro V8 Helmet",
		description: "This helmet has excellent ventilation channels that ensure a smooth flow of air. Maximum impact energy dispersal is also ensured. Whether you're carving a path through the middle of the ice or squaring up on the corners, you can rely on the TACKS 720 to keep you calm and focused while safeguarding your most precious possession.",
		price: 12000,
		onOffer: true
	},
	{

		id: "652524a28e09243c18754d06",
		name: "Hockey Pro V8 Pants",
		description: "Move. Faster. Instinctively.  Thanks to the incorporation of AER-TEC technology into the existing timeless design, hockey pants are the pinnacle of lightweight and flexible protective apparel. While the anti-twist belt keeps these breezers in place as you blow past your opponents, strategic ventilation improves moisture evaporation.",
		price: 11500,
		onOffer: true
	},
	{

		id: "652525398e09243c18754d09",
		name: "Hockey Pro V8 Shin Guards",
		description: "Optimal comfort is guaranteed by a light fit that doesn't sacrifice protection. They are made for casual players who want freedom of movement while protecting themselves on the ic thanks to their design and construction.",
		price: 2750,
		onOffer: true
	},
	{

		id: "652526198e09243c18754d0c",
		name: "Hockey Pro V8 Shoulder Pad",
		description: "With shoulder pads, you can keep your torso bone dry and your head firmly in the game. Their ground-breaking AER-TEC design effectively removes humidity, and their incredibly light EPP foam core completely protects against impact, so there won't be any technical difficulties getting in the way of your success.",
		price: 2750,
		onOffer: true
	},
	{

		id: "652526938e09243c18754d0f",
		name: "Hockey Pro V8 Skates",
		description: "Their feet fit it perfectly thanks to its anatomical design, and the reinforced boot gives them the support they need to perform at their absolute best.",
		price: 11500,
		onOffer: true
	},
	{

		id: "652527238e09243c18754d12",
		name: "Hockey Pro V8 Socks",
		description: "The 100% breathable polyester fabric of the high-performance practice sock is equipped with Velcro fasteners to ensure that it stays in place while also wicking moisture away from your feet. These hockey socks are made to keep you feeling light and comfortable, and they have inserts that improve stretch and mobility.",
		price: 1650,
		onOffer: true
	},
	{

		id: "652529a9a668cf9ec915d831",
		name: "Hockey Shin Guards Jr-SR",
		description: "Designed for occasional or casual players, but they still offer the same levels of comfort and mobility, with PE inserts and easily-adjustable components for a custom fit.",
		price: 3300,
		onOffer: true
	},
	{

		id: "65252b08a668cf9ec915d838",
		name: "Hockey C-Pro V70",
		description: "Maintaining mental clarity to perform at your best throughout the entire game. At every faceoff, you'll draw attention thanks to your elite level protection and comfort.",
		price: 3750,
		onOffer: true
	},
	{

		id: "6526712068823bbb2774ce8b",
		name: "Hockey Pro V70 Helmet",
		description: "Maintaining mental clarity to perform at your best throughout the entire game. At every faceoff, you'll draw attention thanks to your elite level protection and comfort.",
		price: 3750,
		onOffer: true
	},
]

export default productsData;